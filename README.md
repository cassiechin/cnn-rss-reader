# README #
Cassie Chin  
CSCE 4444 Homework 3  

This program visits the CNN website and pulls the titles of the current news articles from the categories of:  

* Entertainment
* Politics
* Technology

(Note: You must have Python 2.7+ installed to use this program.)

### What is this repository for? ###

* This repository holds the python script that does the actual scraping as well as a bash script that installs the required dependencies and executes the python script.

### How do I get set up and run the program? ###

* All setup, configuration, dependencies are handled with the bash script.
* Run this program by executing ./run in the terminal