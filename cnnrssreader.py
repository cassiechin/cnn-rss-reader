#!/usr/bin/python
################################################################################
# Cassie Chin, cassiechin9793@gmail.com, 10/17/14
# CSCE 4444 Homework 3
#
# Visit the CNN website and pull the titles of the current news articles from 
# the catories of:
#     Entertainment
#     Technology
#     Politics
################################################################################

# Read command line arguments
import sys

# The feedparser provides an easy way to parse an RSS feed
import feedparser

# All article titles found will be printed to this file
#OUTPUT_FILE_NAME = "output.txt"
OUTPUT_FILE_NAME = str(sys.argv[1])

################################################################################
# scrape_page
#     This function will take the url and write the titles of every article on 
#     the page to the output file. This function assumes that the file has 
#     already been opened. Thus, the file open/close operations are done OUTSIDE 
#     this function. 
# 
# @param url The url of the rss feed to scrape
################################################################################
def scrape_page (url):
    # Declare feedparser object
    page = feedparser.parse(url)

    # Print the category title as well as the number of articles in the category
    file.write(page.feed.title + " (" + str(len(page.entries)) + " articles)\n")

    # Print out the title for each article in the category on a different line
    for article in page.entries: file.write(article.title + "\n") 

    # New line to separate categories in the output file
    file.write("\n")

################################################################################
# main
#     The file open and close operations are done before and after the 
#     scrape_page function is called. The scrape_page function is called for
#     each required category as specified by the CSCE 4444 Homework 3 program
#     description
################################################################################
if __name__ == '__main__':
    file = open(OUTPUT_FILE_NAME, "w")
    scrape_page('http://rss.cnn.com/rss/cnn_showbiz.rss')
    scrape_page('http://rss.cnn.com/rss/cnn_tech.rss')
    scrape_page('http://rss.cnn.com/rss/cnn_allpolitics.rss')
    file.close()

